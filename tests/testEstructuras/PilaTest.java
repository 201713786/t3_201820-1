package testEstructuras;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOTrip;

public class PilaTest 
{
	private Stack stack;
	
	@Before
	public void setupEscenario1()
	{
		stack= new Stack<>();
	}
	@Before
	public void setupEscenario2()
	{
		stack= new Stack<>();
		VOTrip elemento = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
		stack.push(elemento);
	}
	@Test
	public void pushTest()
	{
		VOTrip elemento = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
		setupEscenario1();
		try
		{
			stack.push(elemento);
			assertNotNull("Se deberia agregar el trip",stack.pop());

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void deleteTest()
	{
		setupEscenario2();
		try
		{
			stack.pop();
			assertTrue("El tama�o deberia ser 0",stack.isEmpty());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
