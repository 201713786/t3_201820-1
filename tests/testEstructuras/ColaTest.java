package testEstructuras;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Queue;;
import model.vo.VOTrip;

public class ColaTest
{
		private Queue<T> cola;
		
		@Before
		public void setupEscenario1()
		{
			cola= new Queue();
		}
		@Before
		public void setupEscenario2()
		{
			cola= new Queue<>();
			VOTrip elemento = new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
			cola.enqueue(elemento);
		}
		@Test
		public void enqueueTest()
		{
			VOTrip elemento= new VOTrip(17536701,"12/31/2017 23:58","1/1/2018 0:03",3304,284,159,"Claremont Ave & Hirsch St",69,"Damen Ave & Pierce Ave","Subscriber","Male",1988);
			setupEscenario1();
			try
			{
				cola.enqueue(elemento);
				assertNotNull("Se deberia agregar el trip",cola.dequeue());

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		@Test
		public void deleteTest()
		{
			setupEscenario2();
			try
			{
				cola.dequeue();
				assertTrue("El tama�o deberia ser 0",cola.isEmpty());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}

