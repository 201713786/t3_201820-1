package controller;

import java.io.FileNotFoundException;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		String stationsFile = new String("C:/Users/Nico/t3_201820/data/Divvy_Stations_2017_Q3Q4.csv");
		manager.loadStations(stationsFile);
	}
	
	public static void loadTrips() {
		String  tripsFile = new String("C:/Users/Nico/t3_201820/data/Divvy_Trips_2017_Q4.csv");
		manager.loadTrips(tripsFile);
	}
		
	public static IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
}
