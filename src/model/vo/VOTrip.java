package model.vo;

import java.util.Arrays;

/**
 * Representation of a Trip object
 */
public class VOTrip {
	
	private int trip_id;
	
	private String start_time;
	
	private String end_time;
	
	private VOBike bike;
	
	private int id_bike;
	
	private int tripduration;
	
	private int from_station_id;
	
	private String from_station_name;
	
	private int to_station_id;
	
	private String to_station_name;
	
	private String usertype;
	
	private String gender=null;
	
	private int birthyear=1994;
	
	public VOTrip(String pId, String pStart_time, String pEnd_time, String pIdBike, String pSeconds, String pFrom_station_id,
			String pFromStation, String pIdToStation, String pToStation, String usertype, String genero, String a�o) {
		
		trip_id = Integer.parseInt(pId);
		
		start_time = pStart_time;
		
		end_time = pEnd_time;
		
		int parmidbike = Integer.parseInt(pIdBike);
	
		bike = new VOBike(parmidbike);
		
		id_bike = parmidbike;
		
		tripduration = Integer.parseInt(pSeconds);
		
		from_station_id = Integer.parseInt(pFrom_station_id);
		
		from_station_name = pFromStation;
		
		to_station_id = Integer.parseInt(pIdToStation);
		
		to_station_name = pToStation;
		
		this.usertype = usertype;
		
		gender = genero;
		
		birthyear = Integer.parseInt(a�o);
	}

	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return trip_id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return tripduration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return from_station_name;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return to_station_name;
	}


	public int hashCode(){
		return id_bike;
	}
	
	public String toString(){
		return gender;
	}
}
