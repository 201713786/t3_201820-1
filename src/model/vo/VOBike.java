package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {

	private int id;
	
	public VOBike(int pId){
		id = pId;
	}
	
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		return id;
	}	
}
