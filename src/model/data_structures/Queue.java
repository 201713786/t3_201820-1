package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T> {
	
	private int size;
	private Node<T> head;
	private Node<T> tail;
	
	int position = 0;

	public Queue() {
		size = 0;
		this.head = null;
		this.tail = null;
		
	}

	@Override
	public Iterator<T> iterator() {
		
		return new iteratorQ();
	}
	
	public class iteratorQ implements Iterator<T>
	{

		public Node<T> nodoRef;
		
		public iteratorQ()
		{
			if(tail!=null)
				 nodoRef=tail;
		}
		@Override
		public boolean hasNext() {
			
			return position<size;
		}

		@Override
		public T next() {
			
			T item = null;
			try {
				 item = nodoRef.getInfo();
				 nodoRef = nodoRef.getNext();
				 position++;
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
		
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		if(tail == null)
		{
			tail = new Node<T>(t);
			head = tail;
			size++;
		}
		
		else
		{
			Node<T> aux = new Node<T>(t);
			tail.previous = aux;
			aux.next = tail;
			tail = tail.previous;
			size++;
			
		}
		
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		T answer = head.info;
		head = head.previous;
		head.next = null;
		
		return answer;	
	}
	
private class Node<T> {
		
		private T info;
		private Node<T> next;
		private Node<T> previous;
	
		public Node(T info){
			this.info=info;
			next=null;
			previous=null;
		}
		public T getInfo() {
			return info;
		}
		public void setInfo(T info) {
			this.info = info;
		}
		public Node<T> getNext() {
			return next;
		}
		public void setNext(Node<T> next) {
			this.next = next;
		}
		public Node<T> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<T> previous) {
			this.previous = previous;
		}
	
		public boolean hasNext(){
			return next!=null;
		}
	
		public boolean hasPrevious(){
			return next!=null;
		}
	
	
	}

}
