package model.data_structures;

import java.util.Iterator;

import model.data_structures.Queue.Node;

public class Stack<T> implements IStack<T> {

	private int size;
	private Node<T> cabeza;
	private Node<T> cola;
	
	int position = 0;
	
	public Stack( ){
		size = 0;
		this.cabeza = null;
		this.cola = null;
		
	}
	@Override
	public Iterator<T> iterator() {
		
		return new iteratorS();
	}
	
	public class iteratorS implements Iterator<T>
	{

		public Node<T> nodoRef;
		
		public iteratorS()
		{
			if(cabeza!=null)
				 nodoRef=cabeza;
		}
		@Override
		public boolean hasNext() {
			
			
			return position>size;
		}

		@Override
		public T next() {
			
			T item = (T) nodoRef.getPrevious();
			return item;
		}
		
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void push(T t) {
		if(cabeza==null){
			
			cabeza = new Node<T>(t);
			cola = cabeza;
			size++;
		}
		else
		{
			
			size++;
			Node<T> aux =new Node<T>(t); 
			cola.next= aux;
			aux.previous=cola;
			cola = cola.next;
			
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public T pop() {
		T ans = cola.info;
		cola = cola.previous;
		if(cola!=null){
		cola.next=null;
		}
		return ans;
	}

	private class Node<T> {
	
		private T info;
		private Node<T> next;
		private Node<T> previous;
	
		public Node(T info){
			this.info=info;
			next=null;
			previous=null;
		}
		public T getInfo() {
			return info;
		}
		public void setInfo(T info) {
			this.info = info;
		}
		public Node<T> getNext() {
			return next;
		}
		public void setNext(Node<T> next) {
			this.next = next;
		}
		public Node<T> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<T> previous) {
			this.previous = previous;
		}
	
		public boolean hasNext(){
			return next!=null;
		}
	
		public boolean hasPrevious(){
			return next!=null;
		}
	
	
	}

}
