package model.data_structures;

public class Nodo <T> {

	private Nodo<T> siguiente= null;
	
	private Nodo<T> anterior;
	
	private T elemento=null;
	
//	private int id;
//	
//	private String name;
//	
//	private String city;
//	
//	private double latitude;
//	
//	private double longitude; 
//	
//	private int dpcapacity;
//	
//	private String online_date;
//	
//	public Nodo(int pId,String pName,String pCity, double pLatitude, double pLongitude, int pDpcapacity, String pOnline_date,Nodo pAnterior) {
//		id = pId;
//		name = pName;
//		city = pCity;
//		anterior = pAnterior;
//	}
	public Nodo(T pElemento)
	{
		elemento = pElemento;
		anterior = null;
	}
	
	public T getItem() {
		return elemento;
	}
	
	public Nodo<T> darSiguiente(){
		return siguiente;
	}
	
	public Nodo<T> darAnterior(){
		return anterior;
	}
	public void cambiarSiguiente(Nodo<T> newSiguiente){
		siguiente = newSiguiente;
	}
	
	public void cambiarAnterior(Nodo<T> newAnterior){
		anterior = newAnterior;
	}
		
	public String toString() {
	    return elemento.toString();
	    }
}
