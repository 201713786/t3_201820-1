package model.logic;

import java.io.*;
import java.util.*;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList<VOTrip> trips = new DoublyLinkedList<VOTrip>();
	
	Iterator<VOTrip> it = trips.iterator();
	
	public DivvyTripsManager(){
	}

	
	public void loadStations (String stationsFile) {
		
		try {
			
			BufferedReader bufferLectura = new BufferedReader(new FileReader(stationsFile));
				
			String linea = bufferLectura.readLine();
				
			while (linea != null) {
				String[] datos = linea.split(String.valueOf(","));
				if(datos[0].equals("id")){}
				else
					System.out.println(Arrays.toString(datos));
				
				linea = bufferLectura.readLine();	
			}
	
			if (bufferLectura != null) {
				bufferLectura.close();
				}
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		
	}
	
	Stack<VOTrip> stack = new Stack<VOTrip>();
	
	Queue<VOTrip> queue = new Queue<VOTrip>();
	
	public void loadTrips (String tripsFile) {
		
		try {
			BufferedReader bufferLectura = new BufferedReader(new FileReader(tripsFile));

			String linea = bufferLectura.readLine();
				
			while (linea != null) {
				String[] datos = linea.split(",");
				if(datos[0].equals("trip_id")){}
				
				else{
					try
					{
						String f=datos[10];
						trips.agregar(new VOTrip(datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],datos[7],datos[8],datos[9],datos[10],datos[11]));

					}
					catch(Exception e)
					{
						trips.agregar(new VOTrip(datos[0],datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],datos[7],datos[8],datos[9],"Male","1994"));

					}
				}
				
				linea = bufferLectura.readLine();	
			}

			if (bufferLectura != null) {
				bufferLectura.close();
				}
			
			} catch (Exception e) {

				e.printStackTrace();
			}
		
		tripsFile = "data/Divvy_Trips_2017_Q3.csv";
		File file = new File(tripsFile);
		
		try {
			Scanner inputStream1 = new Scanner(file);
			
			inputStream1.next();
			
			while(inputStream1.hasNext())
			{
				
				String data = inputStream1.next()+inputStream1.nextLine();
				//System.out.println(data);
				String[] dataS = data.split(",");
				
				int idTrip = Integer.parseInt(dataS[0].substring(1, dataS[0].length()-1));
				dataS[4]=dataS[4].substring(1, dataS[4].length()-1);
				double timeTrip = Double.parseDouble(dataS[4].substring(1, dataS[4].length()-1));
				String tripStart = dataS[5];
				String tripEnd = dataS[7];
				
				VOTrip trippy = new VOTrip( dataS[0],dataS[1],dataS[2],dataS[3],dataS[4],dataS[5],dataS[6],dataS[7],dataS[8],dataS[9],dataS[10],dataS[11]);

				
				
				stack.push(trippy);
				
				
			}
			inputStream1.close();
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		try {
			Scanner inputStream2 = new Scanner(file);
			
			inputStream2.next();
			
			
			
			while(inputStream2.hasNext())
			{
				String data2 = inputStream2.next()+inputStream2.nextLine();
				System.out.println(data2);
				
				String[] dataQ = data2.split(",");
				
				int idTrip = Integer.parseInt(dataQ[0].substring(1, dataQ[0].length()-1));
				dataQ[4]=dataQ[4].substring(1, dataQ[4].length()-1);
				double timeTrip = Double.parseDouble(dataQ[4].substring(1, dataQ[4].length()-1));
				String tripStart = dataQ[5];
				String tripEnd = dataQ[7];
				
				VOTrip trippy2 = new VOTrip(dataQ[0],dataQ[1],dataQ[2],dataQ[3],dataQ[4],dataQ[5],dataQ[6],dataQ[7],dataQ[8],dataQ[9],dataQ[10],dataQ[11]);
				
				queue.enqueue(trippy2);
				
				System.out.println(queue.size());
				
			}
			inputStream2.close();
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		
		DoublyLinkedList<String> resp = new DoublyLinkedList<String>();
		int cont=0;
		Iterator<VOTrip> t = trips.iterator();
		while(t.hasNext() && cont != n){
			try {
				if(t.next().hashCode()==bicycleId){
					cont++;
					resp.agregar(t.next().getToStation());
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return resp;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		
		return null;
	}	


}
